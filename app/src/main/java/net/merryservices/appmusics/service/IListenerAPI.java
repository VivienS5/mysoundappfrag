package net.merryservices.appmusics.service;

import net.merryservices.appmusics.model.Music;

import java.util.ArrayList;

public interface IListenerAPI {

    public void onReceiveMusics(ArrayList<Music> musics);
}
