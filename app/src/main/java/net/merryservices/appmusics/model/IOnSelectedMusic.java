package net.merryservices.appmusics.model;

public interface IOnSelectedMusic {

    public void onSelectedMusic(Music music);
}
