package net.merryservices.appmusics.model;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import net.merryservices.appmusics.database.FavoriteRepository;
import net.merryservices.appmusics.R;
import net.merryservices.appmusics.service.ServiceApi;

import java.util.ArrayList;

public class MusicAdapter extends BaseAdapter {

    private Activity activity;
    private ArrayList<Music> musics;

    public MusicAdapter(Activity activity, ArrayList<Music> musics) {
        this.activity = activity;
        this.musics = musics;
    }

    @Override
    public int getCount() {
        return musics.size();
    }

    @Override
    public Object getItem(int position) {
        return musics.get(position);
    }

    @Override
    public long getItemId(int position) {
        return musics.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null){
            convertView = LayoutInflater.from(activity).inflate(R.layout.item_music, parent, false);
        }
        TextView textViewArtist= convertView.findViewById(R.id.textViewItemArtist);
        textViewArtist.setText(musics.get(position).getArtist());
        TextView textViewTitle= convertView.findViewById(R.id.textViewItemTitle);
        textViewTitle.setText(musics.get(position).getTitle());
        ImageView imageViewItem= convertView.findViewById(R.id.imageViewItemPicture);
        ServiceApi.loadImage(activity, musics.get(position).getImage(), imageViewItem);

        ImageView imageViewFav= convertView.findViewById(R.id.imageViewItemIsFav);
        if(FavoriteRepository.getInstance(activity).isFavorite(musics.get(position))){
            imageViewFav.setImageResource(android.R.drawable.star_big_on);
        }else{
            imageViewFav.setImageResource(android.R.drawable.star_big_off);
        }
        return convertView;
    }
}
