package net.merryservices.appmusics.model;

import org.json.JSONException;
import org.json.JSONObject;

public class MusicHelper {

    public static Music jsonToMusic(JSONObject object){
        Music temp= new Music();
        try {
            temp.setId(object.getInt("id"));
            temp.setTitle(object.getString("title"));
            temp.setArtist(object.getJSONObject("artist").getString("name"));
            temp.setAlbum(object.getJSONObject("album").getString("title"));
            temp.setImage(object.getJSONObject("album").getString("cover_medium"));
            temp.setPreview(object.getString("preview"));
            temp.setLink(object.getString("link"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return temp;
    }
}
