package net.merryservices.appmusics.fragment;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import net.merryservices.appmusics.R;
import net.merryservices.appmusics.database.FavoriteRepository;
import net.merryservices.appmusics.model.Music;
import net.merryservices.appmusics.service.ServiceApi;

import java.io.IOException;

public class MusicFragment extends Fragment implements View.OnClickListener {

    TextView textViewTitle, textViewArtist, textViewAlbum;
    ImageView imageViewAlbum;
    Button buttonListen, buttonLink;
    Music currentMusic;
    MediaPlayer player= new MediaPlayer();
    RadioButton buttonFavYes, buttonFavNo;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup group, Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.fragment_music, null);

        textViewTitle= v.findViewById(R.id.textViewMusicTitle);
        textViewArtist= v.findViewById(R.id.textViewFieldArtiste);
        textViewAlbum= v.findViewById(R.id.textViewFieldAlbum);
        imageViewAlbum= v.findViewById(R.id.imageViewAlbum);
        buttonListen= v.findViewById(R.id.buttonListen);
        buttonLink= v.findViewById(R.id.buttonLinkWeb);
        buttonListen.setOnClickListener(this);
        buttonLink.setOnClickListener(this);

        buttonFavNo= v.findViewById(R.id.radioButtonFavNo);
        buttonFavYes= v.findViewById(R.id.radioButtonFavYes);

        buttonFavYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonFavYes.setChecked(true);
                buttonFavNo.setChecked(false);
                FavoriteRepository.getInstance(getContext()).add(currentMusic);
            }
        });
        buttonFavNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonFavNo.setChecked(true);
                buttonFavYes.setChecked(false);
                FavoriteRepository.getInstance(getContext()).remove(currentMusic);
            }
        });
        return v;
    }

    public void setCurrentMusic(Music music){
        this.currentMusic= music;
        refresh();
    }

    private void refresh() {
        if(currentMusic!=null){
            textViewTitle.setText(currentMusic.getTitle());
            textViewArtist.setText(currentMusic.getArtist());
            textViewAlbum.setText(currentMusic.getAlbum());
            ServiceApi.loadImage(getContext(), currentMusic.getImage(), imageViewAlbum);
            if(FavoriteRepository.getInstance(getContext()).isFavorite(currentMusic)){
                buttonFavYes.setChecked(true);
                buttonFavNo.setChecked(false);
            }else{
                buttonFavNo.setChecked(true);
                buttonFavYes.setChecked(false);
            }
        }
    }

    @Override
    public void onClick(View v) {
        if(v.equals(buttonLink)){
            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse(currentMusic.getLink()));
            startActivity(intent);
        }else{
            if(!player.isPlaying()){
                buttonListen.setText("STOP");
                try {
                    player.reset();
                    player.setDataSource(getContext(), Uri.parse(currentMusic.getPreview()));
                    player.prepare();
                    player.start();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }else{
                buttonListen.setText("Ecouter");
                player.stop();
            }
        }

    }
}
